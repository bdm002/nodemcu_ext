// library추가: dht, unified, opendevice

#include <SimpleDHT.h>

// for DHT11, 
//      VCC: 5V or 3V
//      GND: GND
//      DATA: 5
int pinDHT11 = 2;  // D4
SimpleDHT11  dht11;

void setup() {
  Serial.begin(115200);
}

void loop() {
  
  Serial.println("=================================");
  Serial.println("Sample DHT11...");
  
  float temperature = 0;
  float humidity = 0;
  int err = SimpleDHTErrSuccess;
  if ((err = dht11.read2(pinDHT11, &temperature, &humidity, NULL)) != SimpleDHTErrSuccess) {
    Serial.print("Read DHT11 failed, err="); Serial.println(err);delay(2000);
    return;
  }
  
  Serial.print("Sample OK: ");
  Serial.print((float)temperature); Serial.print(" *C, ");
  Serial.print((float)humidity); Serial.println(" RH%");
  
  // DHT11 sampling rate is 0.5HZ.
  delay(2500);
}

