#define PIR_PIN  4    //D2
#define LED_PIN  16      //D0

void setup() { 
  Serial.begin(115200);
  pinMode(PIR_PIN, INPUT);
  pinMode(LED_PIN, OUTPUT);
}
 
void loop() {
int  coming = digitalRead(PIR_PIN);
Serial.println(coming);
 
  if (coming == 1) { 
    digitalWrite(LED_PIN, HIGH);
    delay(3000);
  } else {
    digitalWrite(LED_PIN, LOW);
  }
}

