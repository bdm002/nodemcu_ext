#include "ESP8266WiFi.h"
#include <ESP8266WebServer.h>
#include <SimpleDHT.h>

int pinDHT11 = 2;  // D4
SimpleDHT11  dht11;

// char *ssid = "AndroidHotspot3557";
// char *password = "";
char *ssid = "olleh_WiFi_D25A";
char *password = "0000000433";


ESP8266WebServer server(80);

void handleRoot() {
  Serial.println("=================================");
  Serial.println("Sample DHT11...");
  
  float t = 0;
  float h = 0;
  int err = SimpleDHTErrSuccess;
  if ((err = dht11.read2(pinDHT11, &t, &h, NULL)) != SimpleDHTErrSuccess) {
    Serial.print("Read DHT11 failed, err="); Serial.println(err);delay(2000);
    return;
  }
  
  Serial.print("Sample OK: ");
  Serial.print((float)t); Serial.print(" *C, ");
  Serial.print((float)h); Serial.println(" RH%");

  String message = "";
  message += "Temperature : ";
  message += t;
  message += ", Humidity:  ";
  message +=h;

  server.send(200, "text/plain", message);
  
  // DHT11 sampling rate is 0.5HZ.
  delay(2500);
}

void setup(void) {
  Serial.begin(115200);

  Serial.print("Connecting to \'");
  Serial.print(ssid);
  Serial.println("\'");

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println();
  Serial.println("WiFi connected");

  Serial.print("Server IP address:  ");
  Serial.println(WiFi.localIP());

  server.on("/", handleRoot);

  server.begin();
  Serial.println("HTTP server started");
}

void loop(void) {
  server.handleClient();
}

