#include "ESP8266WiFi.h"
#include <ESP8266WebServer.h>

char *ssid = "AndroidHotspot3557";
char *password = "";


int LED_pin = 16;         //D0
boolean LED_state = false;

ESP8266WebServer server(80);

void setup(void) {
  Serial.begin(115200);

  Serial.print("Connecting to \'");
  Serial.print(ssid);
  Serial.println("\'");

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println();
  Serial.println("WiFi connected:");

  Serial.print("Server IP address: ");
  Serial.println(WiFi.localIP());

  server.on("/", handleRoot);
  server.on("/led.cgi", handleONOFF);

  server.begin();
  Serial.println("HTTP server started");

  pinMode(LED_pin, OUTPUT);
  digitalWrite(LED_pin, LED_state);
}

void loop(void) {
  server.handleClient();
}

void handleRoot() {
  String message = "";
  message += "<html>";
  message += "<body>";

  message += "Currently... LED is ";
  message += (LED_state ? "ON." : "OFF");

  message += "<br />";
  message += "<FORM method = \"get\"  action=\"/led.cgi\">";
  message += "<P><INPUT type=\"radio\" name=\"LEDstatus\"  value=\"1\">Turn ON";
  message += "<P><INPUT type=\"radio\" name=\"LEDstatus\"  value=\"0\">Turn OFF";
  message += "<P><INPUT type=\"submit\" value=\"Submit\"> </FORM>";

  message += "</body>";
  message += "</html>";

  server.send(200, "text/html", message);
}

void handleONOFF() {
  if(server.argName(0) == "LEDstatus") {
    int state = server.arg(0).toInt();

    LED_state = state;
    digitalWrite(LED_pin, LED_state);

    String message = "";
    message += "<html>";
    message += "<body>";
    message += "Now... LED is";
    message += (LED_state ? "ON.": "OFF.");
    message += "<br/><br />";
    message += "<a href = \"/\"> Main Control Page</a>";

    message += "</body>";
    message += "</html>";

    server.send(200, "text/html", message);
  }
}


