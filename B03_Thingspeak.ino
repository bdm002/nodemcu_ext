#include <SimpleDHT.h>
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
char *ssid = "olleh_WiFi_D25A";
char *password = "0000000433";

int pinDHT22 = 5;  // D1
SimpleDHT22 dht22;

void setup() {
  Serial.begin(115200);

  WiFi.mode(WIFI_STA);
  WiFi.disconnect();

  Serial.print("Connecting to \'");
  Serial.print(ssid);
  Serial.println("\'");

  WiFi.begin(ssid, password);

  while(WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println();
  Serial.println("WiFi connected.");
}


void loop() {
  float light = 0;
  float temperature = 0;
  float humidity = 0;
 
  int err = SimpleDHTErrSuccess;
  light = analogRead(A0);
  if ((err = dht22.read2(pinDHT22, &temperature, &humidity, NULL)) != SimpleDHTErrSuccess) {
    Serial.print("Read DHT22 failed, err =");
    Serial.println(err); delay(2000);
    return;
     }
  HTTPClient httpClient;
  String host = "http://api.thingspeak.com";
  String url = "/update?key=";
  String key = "8Y6HITWH0Z2Q7U9D";
  String field1 = "&field1=";
  String field2 = "&field2=";
  String field3 = "&field3=";

  httpClient.begin(host + url + key + field1 + light + field2 + temperature + field3 + humidity);
  int httpCode = httpClient.GET();

  if(httpCode > 0) {
    Serial.printf("[HTTP] Temperature & Humidity data was logged... \n");
  } else {
    Serial.printf("[HTTP] Connection failed : %s \n", httpClient.errorToString(httpCode).c_str());
  }
  httpClient.end();
  delay(20000);
}

