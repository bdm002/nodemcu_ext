#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>

char *ssid = "AndroidHotspot3557";
char *password = "";

void setup(){
    Serial.begin(115200);

    WiFi.mode(WIFI_STA);
    WiFi.disconnect();

    Serial.print("Connecting to \'");
    Serial.print(ssid);
    Serial.println("\n'");

    WiFi.begin(ssid, password);

    while (WiFi.status() != WL_CONNECTED) {
      delay(500);
      Serial.print(".");
    }
    Serial.println();
    Serial.println("WiFi connected");
}

void loop() {
  HTTPClient httpClient;

  String host = "http://api.openweathermap.org";
  String url = "/data/2.5/weather?id=1835235&APPID=";
  String key = "3aa579644c0adb5ef89860e17ecfc394";

  httpClient.begin (host + url + key);
  int httpCode = httpClient.GET();

  if (httpCode > 0) {
    Serial.printf("[HTTP] request from the client was handled: %d \n", httpCode);

    if (httpCode == HTTP_CODE_OK) {
      String payload = httpClient.getString();
      processWeatherInformation(payload);
    }

  } else {
        Serial.printf("[HTTP] connection failed: %s \n", httpClient.errorToString (httpCode).c_str());
           }
           httpClient.end();
           delay(5000);
  }
  void processWeatherInformation(String buffer) {
     int index1, index2;
     String fieldInfo;
     char *key1 = "\"weather\"", *key2 = "\"main\"", *key3 ="\"temp\"";

     index1 = buffer.indexOf(key1);
     index2 = buffer.indexOf(key2, index1);
     index1 = buffer.indexOf('\"', index2 + strlen (key2));
     index2 = buffer.indexOf('\"', index1 +1);

     fieldInfo = buffer.substring(index1+1, index2);
     Serial.println();
     Serial.println("Current Weather in Daejeon :    " + fieldInfo);

     index1 = buffer.indexOf(key3, index2);
     index2 = buffer.indexOf(':', index1 + strlen(key3));
     index1 = buffer.indexOf(',', index2 + 1);

     fieldInfo = buffer.substring(index2 + 1, index1);
     float temperature = fieldInfo.toFloat() - 273.15;
     Serial.print ("Current Temperature in Daejeon: ");
     Serial.println(temperature);
     Serial.println( );
}


/*
int indexOf(int ch): It returns the index of the first occurrence of character ch in a String.
int indexOf(int ch, int fromIndex): It returns the index of first occurrence if character ch, starting from the specified index “fromIndex”.
*/