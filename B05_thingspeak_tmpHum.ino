// 한 보드에서 조도센싱값과 온습도 센싱값 3개를 클라우드로 전송

#include <SimpleDHT.h>
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
char *ssid = "olleh_WiFi_D25A";
char *password = "0000000433";

int pinDHT11 = 5;
SimpleDHT11 dht11;

void setup() {
  Serial.begin(115200);

  WiFi.mode(WIFI_STA);
  WiFi.disconnect();

  Serial.print("Connecting to \'");
  Serial.print(ssid);
  Serial.println("\'");

  WiFi.begin(ssid, password);

  while(WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println();
  Serial.println("WiFi connected.");
}


void loop() {
  float temperature = 0;
  float humidity = 0;
 
  int err = SimpleDHTErrSuccess;
  if ((err = dht11.read2(pinDHT11, &temperature, &humidity, NULL)) != SimpleDHTErrSuccess) {
    Serial.print("Read DHT11 failed, err =");
    Serial.println(err); delay(2000);
    return;
     }
  HTTPClient httpClient;
  String host = "http://api.thingspeak.com";
  String url = "/update?key=";
  String key = "8Y6HITWH0Z2Q7U9D";
  String field1 = "&field1=";
  String field2 = "&field2=";

  httpClient.begin(host + url + field1 + temperature + field2 + humidity);
  int httpCode = httpClient.GET();

  if(httpCode > 0) {
    Serial.printf("[HTTP] Temperature & Humidity data was logged... \n");
  } else {
    Serial.printf("[HTTP] Connection failed : %s \n", httpClient.errorToString(httpCode).c_str());
  }
  httpClient.end();
  delay(20000);
}
